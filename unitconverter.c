#include <stdlib.h>
#include <stdio.h>
#include "unitconv_func.h"

int main(int argc, char *argv[]) 
{
	int quantity_switch;
	float input_number,output;
	int INUnit, YUnit;
	char input_YUnit[3], input_INUnit[3];
	PrintLine();
	printf("\n unitconverter - version 0.1.8\n\n");
	PrintLine();
	quantity_switch = Menu(2,Quantities);
	switch (quantity_switch)
	{
		case 1: printf("Enter your unit:\n");
			scanf("%s",&input_YUnit[0]);
			YUnit = CheckUnit(quantity_switch,input_YUnit);
			printf("Enter your value [%s]:\n",input_YUnit);
			scanf("%f",&input_number);
			printf("Enter the unit in which you want to convert [%s]:\n",input_YUnit);	
			scanf("%s",&input_INUnit[0]);
			INUnit = CheckUnit(quantity_switch,input_INUnit);
			if (YUnit == 255 || INUnit == 255) { printf("Error!\n\n"); return EXIT_FAILURE; }
			output = input_number*Length_factors[YUnit][INUnit];
			break;
		case 2:	return EXIT_FAILURE;
		default: printf("Error - Unknown\n");
			return EXIT_FAILURE;
	}
	PrintLine();
	printf(" %s --> %s\n",input_YUnit,input_INUnit);
	printf(" Solution:\n\t%g %s = %g %s\n",input_number,input_YUnit,output,input_INUnit);
	PrintLine();
	printf("\nThank you for using unitconverter!\n\n");
	return EXIT_SUCCESS;
}
